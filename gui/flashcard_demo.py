"""
Demonstration program showing the FlashCard widget.

Usage: flashcard_demo.py <options> [consonant_name]

where <options> is zero or more of:
    -h|--help
        prints this help and stops
"""


import os
import sys
import copy
import getopt
import traceback

# import all PyQt5 stuff, error if we can't
try:
    from PyQt5.QtGui import QPixmap
    from PyQt5.QtWidgets import (QApplication, QMainWindow, QWidget, QFrame,
                                 QAction, QGridLayout, QErrorMessage, QPushButton)
except ImportError:
    msg = '*'*60 + '\nSorry, you must install PyQt5\n' + '*'*60
    print(msg)
    sys.exit(1)

from thai_data import ThaiConsonants

#import flashcard_widget
#FlashCard = flashcard_widget.FlashCard
from flashcard_widget import FlashCard

try:
    import log
    log = log.Log('flashcard_demo.log')
except AttributeError:
    # means log already set up
    pass
except ImportError as e:
    # if we don't have log.py, don't crash
    # fake all log(), log.debug(), ... calls
    def logit(*args, **kwargs):
        pass
    log = logit
    log.debug = logit
    log.info = logit
    log.warn = logit
    log.error = logit
    log.critical = logit


######
# Various demo constants
######

# demo name/version
#DemoName = 'FlashCard %s - Demonstration' % flashcard_widget.__version__
DemoName = 'FlashCard - Demonstration'
DemoVersion = '1.0'

DemoWidth = 100 #500
DemoHeight = 100 #250

###############################################################################
# The main application frame
###############################################################################

class FlashCardDemo(QMainWindow):

    def __init__(self, consonant_list, consonant=None):
        super().__init__()

        self.consonant_list = consonant_list
        self.consonant_index = None

        if consonant in consonant_list:
            self.consonant_index = consonant_list.index(consonant)

        # start the GUI
        grid = QGridLayout()
        grid.setColumnStretch(0, 1)
        grid.setContentsMargins(2, 2, 2, 2)

        qwidget = QWidget(self)
        qwidget.setLayout(grid)
        self.setCentralWidget(qwidget)

        # put a FlashCard into the display
        self.flashcard = FlashCard(consonant)
        grid.addWidget(self.flashcard, 0, 0, 1, 3)

        # create a "line widget"
        line = QFrame(self)
        line.setFrameShape(QFrame.HLine)
        line.setFrameShadow(QFrame.Raised)
        grid.addWidget(line, 2, 0, 1, 4)

        # add the "Prev" button
        self.btn_prev = QPushButton('Prev', self)
        grid.addWidget(self.btn_prev, 3, 1)

        # add the "Next" button
        self.btn_next = QPushButton('Next', self)
        grid.addWidget(self.btn_next, 3, 2)

        # connect 'Next' button to select next consonant
        self.btn_next.clicked.connect(self.next_consonant)
        self.btn_prev.clicked.connect(self.prev_consonant)
#        self.btn_next.clicked.connect(self.mode_all)
#        self.btn_prev.clicked.connect(self.mode_glyph)

        # set the size of the demo window, etc
        self.setGeometry(100, 100, DemoWidth, DemoHeight)
        self.setWindowTitle('%s %s' % (DemoName, DemoVersion))
        self.show()

    def mode_all(self):
        self.flashcard.set_mode(FlashCard.All)

    def mode_glyph(self):
        self.flashcard.set_mode(FlashCard.Glyph)

    def prev_consonant(self):
        """Update widget to show previous consonant in list."""

        if self.consonant_index is None:
            self.consonant_index = len(self.consonant_list)

        self.consonant_index -= 1
        if self.consonant_index < 0:
            self.consonant_index = len(self.consonant_list) - 1
        self.flashcard.update(self.consonant_list[self.consonant_index])

    def next_consonant(self):
        """Update widget to show next consonant in list."""

        if self.consonant_index is None:
            self.consonant_index = -1

        self.consonant_index += 1
        if self.consonant_index >= len(self.consonant_list):
            self.consonant_index = 0
        self.flashcard.update(self.consonant_list[self.consonant_index])

    def onClose(self):
        """Application is closing."""

        pass

        #self.Close(True)

    def hasHeightForWidth(self):
        print(f'hasHeightForWidth: returning True')
        return True

    def heightForWidth(self, width):
        """Set widget height for given width."""

        print(f'heightForWidth: width={width}, returning {width * 2}')

        return width * 2

    ######
    # Small utility routines
    ######

    def unimplemented(self, msg):
        """Issue an "Sorry, ..." message."""

        self.flashcard.warn('Sorry, %s is not implemented at the moment.' % msg)

    ######
    # Warning and information dialogs
    ######

    def info(self, msg):
        """Display an information message, log and graphically."""

        log_msg = '# ' + msg
        length = len(log_msg)
        prefix = '#### Information '
        banner = prefix + '#'*(80 - len(log_msg) - len(prefix))
        log(banner)
        log(log_msg)
        log(banner)

        info_dialog = QErrorMessage(self)
        info_dialog.showMessage(msg)

    def warn(self, msg):
        """Display a warning message, log and graphically."""

        log_msg = '# ' + msg
        length = len(log_msg)
        prefix = '#### Warning '
        banner = prefix + '#'*(80 - len(log_msg) - len(prefix))
        log(banner)
        log(log_msg)
        log(banner)

        warn_dialog = QErrorMessage(self)
        warn_dialog.showMessage(msg)

###############################################################################
# Main code
###############################################################################

def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)

# our own handler for uncaught exceptions
def excepthook(type, value, tback):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tback))
    msg += '=' * 80 + '\n'
    log(msg)
    print(msg)
    sys.exit(1)

# plug our handler into the python system
sys.excepthook = excepthook

# get list of consonant names
consonant_names = ['gor_gai', 'kor_khai', 'khor_khuaat', 'kor_khwaai', 'khor_khohn',
                   'khor_rakhang', 'ngor_nguu', 'jor_jaan', 'chor_ching', 'chor_chaang',
                   'sor_so', 'chor_chuuhr', 'yor_ying', 'dor_chadaa', 'tor_bpadtak',
                   'thor_thaan', 'thor_mohntho', 'thor_phuuthao', 'nor_naehn', 'dor_dek',
                   'dtor_dtao', 'tor_thoong', 'tor_thahaan', 'thor_thohng', 'nor_nuu',
                   'bor_baimaai', 'bpor_bplaa', 'por_pheung', 'for_faa', 'por_phaan',
                   'for_fan', 'phor_samphao', 'mor_maa', 'yor_yak', 'ror_reuua',
                   'lor_ling', 'wor_waaen', 'sor_saalaa', 'sor_reuusee', 'sor_seuua',
                   'hor_heep', 'lor_joolaa', 'or_aang', 'hor_nohkhuuk',
                  ]

# create Cosonants from that list
consonant_list = [ThaiConsonants[name] for name in consonant_names]

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'h', ['help'])
except getopt.error:
    usage()
    sys.exit(1)

for (opt, param) in opts:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)

# check args
name = None
if len(args) == 1:
    name = args[0]
    try:
        name = ThaiConsonants[name]
    except KeyError:
        print(f"Sorry, consonant '{name}' not found.")
        sys.exit(1)
elif len(args) > 1:
    usage()
    sys.exit(1)

# start the app
app = QApplication(args)
ex = FlashCardDemo(consonant_list, name)
sys.exit(app.exec_())

