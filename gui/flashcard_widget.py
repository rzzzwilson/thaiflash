"""
FlashCard widget.

This is used to display the flashcard information:

    +--------------------------------------------------------+
    |  +---------------+ +----------------++----------------+|
    |  |               | | .class         || .tl_glyph_examp||
    |  |               | +----------------++----------------+|
    |  |               |                   +----------------+|
    |  |               |                   | .meaning       ||
    |  | .glyph        |                   +----------------+|
    |  |               | +----------------++----------------+|
    |  |               | | .glyph_example || .play_sound    ||
    |  |               | +----------------++----------------+|
    |  |               |                                     |
    |  |               |                                     |
    |  |               |                   +----------------+|
    |  |               |                   |.initial_final  ||
    |  +---------------+                   +----------------+|
    +--------------------------------------------------------+

Constructor:

    fc = FlashCard(parent, consonant=None, *args, **kwargs)

where parent         is the owning window
      consonant      is a Consonant object to populate with
      args & kwargs  are extra QWidget parameters

Methods:

    fc.clear()              # remove all consonant display data
    fc.update(consonant)    # change consonant being shown
    fc.set_mode(mode)       # set widget mode (.All or .Glyph)
    fc.play_sound(event)    # play the sound file for the acrophonic
"""


from PyQt5.QtGui import QPixmap, QFont, QIcon
from PyQt5.QtWidgets import (QWidget, QLabel, QGridLayout, QPushButton, QSizePolicy, QToolTip)
from PyQt5 import QtCore
from PyQt5.QtMultimedia import QSound
from PyQt5 import QtMultimedia

import sys
import os.path
import platform

if platform.python_version_tuple()[0] != '3':
    msg = ('You must run flashcard_widget with python 3.x, you are running version %s.x.'
            % platform.python_version_tuple()[0])
    log(msg)
    print(msg)
    sys.exit(1)

try:
    import log
    log = log.Log('flashcard_demo.log')
except AttributeError:
    # means log already set up
    pass
except ImportError as e:
    # if we don't have log.py, don't crash
    # fake all log(), log.debug(), ... calls
    def logit(*args, **kwargs):
        pass
    log = logit
    log.debug = logit
    log.info = logit
    log.warn = logit
    log.error = logit
    log.critical = logit

# version number of the widget
__version__ = '0.2'


# set platform dependant values
if platform.system() == 'Linux':
    WavDataPath = './data/consonants'
elif platform.system() == 'Darwin':
    WavDataPath = './data/consonants'
elif platform.system() == 'Windows':
    raise Exception("Sorry, Windows isn't supported")
else:
    raise Exception('Unrecognized platform: %s' % platform.system())


class FlashCard(QWidget):
    """A widget to display a flashcard."""

    # flashcard modes
    All = 0         # show all fields
    Glyph = 1       # only glyph shows

    DefaultGlyphSize = 200
    DefaultGlyphExampleSize = 40
    DefaultTLExampleSize = 20
    DefaultClassSize = 20
    DefaultMeaningSize = 12
    DefaultInitialFinalSize = 20
#    DefaultFont = "Arial"
    DefaultFont = None
#    ToolTipFont = 'SansSerif'
    ToolTipFont = None

    # set platform dependant values
    if platform.system() == 'Linux':
        pass
    elif platform.system() == 'Darwin':
        pass
    elif platform.system() == 'Windows':
        pass
    else:
        raise Exception('Unrecognized platform: %s' % platform.system())

    # some stylesheet data for widget components
    GlyphStyle = 'background-color: white; border-radius: 9px;'
    ConsonantStyle = 'background-color: white; border-radius: 7px;'
    GlyphExampleStyle = 'background-color: white; border-radius: 7px;'
    LabelStyle = ('border:1px solid rgb(128, 128, 128); '
                  'border-radius: 5px; ')
    ButtonStyle = 'border: 1px dashed gray; border-radius: 7px; '


    def __init__(self, consonant=None, *args, **kwargs):
        """Initialize the flashcard widget.

        consonant      a Consonant object
        args & kwargs  extra QWidget args
        """

        super().__init__(*args, **kwargs)    # inherit all parent object setup
        self.setAutoFillBackground(True)

        # initialize list of label widgets that can be blanked
        self.label_widgets = []

        # remember widget stuff
        self.consonant = consonant

        # set widget size
        self.setMinimumSize(700, 500)
        self.setMaximumSize(700, 500)

        QToolTip.setFont(QFont(FlashCard.ToolTipFont, 10))

        # the large display of the consonant
        self.lbl_glyph = QLabel(self)
        self.lbl_glyph.setFixedWidth(250)
        self.lbl_glyph.setFixedHeight(500)
        self.lbl_glyph.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignTop)
        font = QFont(FlashCard.DefaultFont, FlashCard.DefaultGlyphSize)
        self.lbl_glyph.setFont(font)
        self.lbl_glyph.setStyleSheet(FlashCard.GlyphStyle)
        self.lbl_glyph.setToolTip('The consonant character')

        # the consonant class
        self.lbl_class = QLabel(self)
        font = QFont(FlashCard.DefaultFont, FlashCard.DefaultClassSize)
        self.lbl_class.setFont(font)
        self.lbl_class.setAlignment(QtCore.Qt.AlignBottom)
        self.lbl_class.setToolTip('The consonant class')

        # the example of usage
        self.lbl_tl_glyph_example = QLabel(self)
        font = QFont(FlashCard.DefaultFont, FlashCard.DefaultTLExampleSize)
        self.lbl_tl_glyph_example.setFont(font)
        self.lbl_tl_glyph_example.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignRight)
        self.lbl_tl_glyph_example.setToolTip('Consonant and example word transliteration')

        # the English meaning
        self.lbl_meaning = QLabel(self)
        font = QFont(FlashCard.DefaultFont, FlashCard.DefaultMeaningSize)
        self.lbl_meaning.setFont(font)
        self.lbl_meaning.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        self.lbl_meaning.setToolTip('The English meaning of the example word)')

        # the Thai acrophonic dislay
        # self.CreatorL.setGeometry(QtCore.QRect(70, 80, 100, 100)) #(x, y, width, height)
        self.lbl_glyph_example = QLabel(self)
        self.lbl_glyph_example.setContentsMargins(0, 0, 0, 0)
        font = QFont(FlashCard.DefaultFont, FlashCard.DefaultGlyphExampleSize) #, QFont.Bold)
        self.lbl_glyph_example.setFont(font)
#        self.lbl_glyph_example.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignTop)
        self.lbl_glyph_example.setStyleSheet(FlashCard.GlyphExampleStyle)
        self.lbl_glyph_example.setContentsMargins(5, 5, 5, 5)
        self.lbl_glyph_example.setToolTip('Consonant and example word - acrophonic')

        # the English sounds display
        self.lbl_initial_final = QLabel(self)
        font = QFont(FlashCard.DefaultFont, FlashCard.DefaultInitialFinalSize)
        self.lbl_initial_final.setFont(font)
        self.lbl_initial_final.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        self.lbl_initial_final.setToolTip('Sounds at beginning and end of word')

        # the little "speaker" button to play the sound
        self.btn_play_sound = QPushButton()
        self.btn_play_sound.setFixedSize(QtCore.QSize(40, 40))
        self.btn_play_sound.setIcon(QIcon(os.path.join('images', 'speaker_small.png')))
        self.btn_play_sound.setStyleSheet(FlashCard.ButtonStyle)
        self.btn_play_sound.setToolTip('Hear the pronunciation of the acrophonic')

        # start layout
        grid = QGridLayout()
        self.setLayout(grid)
        grid.setContentsMargins(5, 5, 5, 5)
        grid.addWidget(self.lbl_glyph, 0, 0, 6, 1)
        grid.addWidget(self.lbl_class, 0, 1)
        grid.addWidget(self.lbl_tl_glyph_example, 0, 2)
        grid.addWidget(self.lbl_meaning, 1, 2)
        grid.addWidget(self.lbl_glyph_example, 3, 1)
        grid.addWidget(self.btn_play_sound, 3, 2)
        grid.addWidget(self.lbl_initial_final, 5, 2)

        # fill background - debug
#        p = self.palette()
#        p.setColor(self.backgroundRole(), QtCore.Qt.lightGray)
#        self.setPalette(p)

        # if consonant given, populate widget
        if consonant:
            self.update(consonant)
        else:
            self.clear()

        self.show()

    def clear(self):
        """Set all text fields to "empty"."""

        self.lbl_glyph.setText('')
        self.lbl_class.setText('')
        self.lbl_tl_glyph_example.setText('')
        self.lbl_meaning.setText('')
        self.lbl_glyph_example.setText('')
        self.lbl_initial_final.setText('')

    def update(self, consonant):
        """Update consonant in widget."""

        self.consonant = consonant

        # update text fields
        self.lbl_glyph.setText(f'{self.consonant.glyph}')
        self.lbl_glyph.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignTop)
        self.lbl_class.setText('class')
        self.lbl_tl_glyph_example.setText(f'{self.consonant.glyph}.{self.consonant.example}')
        self.lbl_meaning.setText(self.consonant.meaning[0])
        self.lbl_glyph_example.setText(f'{self.consonant.glyph}.{self.consonant.example}')
        self.lbl_initial_final.setText(f'[{self.consonant.initial}]  [{self.consonant.final}]')

        # get the sound data for this consonant
        sound_file = os.path.join(WavDataPath, consonant.sound_file)
        self.sound = QSound(sound_file)

        # connect internal widget events to correct sound handler
        self.btn_play_sound.clicked.connect(self.play_sound)

    def set_mode(self, mode):
        """Set the widget mode.

        mode  the mode to set
              (.All, .Glyph)
        """

        if mode == FlashCard.All:
            self.lbl_class.setText('')
            self.lbl_tl_glyph_example.setText('')
            self.lbl_meaning.setText('')
            self.lbl_glyph_example.setText('')
            self.lbl_initial_final.setText('')
            self.btn_play_sound.setEnabled(False)
        elif mode == FlashCard.Glyph:
            self.update(self.consonant)
#            self.lbl_class.setText('')
#            self.lbl_tl_glyph_example.setText('')
#            self.lbl_meaning.setText('')
#            self.lbl_glyph_example.setText('')
#            self.lbl_initial_final.setText('')
            self.btn_play_sound.setEnabled(True)
        else:
            self.mode = mode
            print(f'Bad mode: {mode}')

    def play_sound(self, event):
        """Play the sound associated with this consonant."""

        self.sound.play()
