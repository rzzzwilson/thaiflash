"""
Define the Classes holding everything needed for Thai flash cards.
"""

import os.path
import time
import platform

if platform.system() == 'Linux':
    ConsonantSoundsDirectory = './data/consonants'
elif platform.system() == 'Darwin':
    ConsonantSoundsDirectory = './data/consonants'
elif platform.system() == 'Windows':
    raise Exception("Windows isn't handled.")
else:
    raise Exception('Unrecognized platform: %s' % platform.system())


class ThaiCard:
    """Base card for Thai consonant and vowel flashcards."""

    def __init__(self, glyph, example, description, translit, phonetic,
                       sound_file, sound_dir):
        """
        A generic flashcard.

        glyph        the consonant, Unicode string
        example      example usage
        description  description of glyph (class, tone, etc)
        translit     transliterated English from 'example'
        phonetic     phonetic symbol for sound
        sound_file   name of audio file
        sound_dir    path to the sound file directory
        """

        self.glyph = glyph
        self.example = example
        self.description = description
        self.translit = translit
        self.phonetic = phonetic
        self.sound_file = sound_file
        self.sound_dir = sound_dir

    def say(self):
        sound_path = os.path.join(self.sound_dir, self.sound_file)
        playsound(sound_path)
        time.sleep(0.5)


class Consonant(ThaiCard):
    """Class holding all consonant information."""

    DefaultSoundsDir = './data/consonants'

    def __init__(self, glyph, example, description, translit, phonetic,
                       meaning, initial, final, sound_file, sound_dir=None):
        """
        Create a Thai consonant flashcard.

        glyph        the consonant, Unicode string
        example      glyph example
        description  description of consonant (class)
        translit     transliteral string for example
        phonetic     phonetic symbol for sound
        meaning      English meaning of 'example'
        initial      sound when consonant is in initial position
        final        sound when consonant is in final position
        sound_file   name of audio file
        sound_dir    path to the (optional) directory of sound files
        """

        if sound_dir is None:
            sound_dir = Consonant.DefaultSoundsDir

        super().__init__(glyph, example, description, translit, phonetic,
                         sound_file, sound_dir)
        self.sound_file = sound_file
        self.sound_dir = sound_dir
        self.meaning = meaning
        self.initial = initial
        self.final = final

    def __str__(self):
        return f'<Consonant: {self.glyph}.{self.example} {self.translit}>'

    def __repr__(self):
        return self.__str__()

class Vowel(ThaiCard):
    """Class holding all vowel information."""

    DefaultSoundsDir = './data/vowels'

    def __init__(self, glyph, example, description, translit, phonetic, sound_file, sound_dir=None):
        """
        Create a Thai vowel flashcard.

        glyph        the consonant, Unicode string
        example      glyph in Thai
        description  description of consonant (class)
        translit     English transliteration
        phonetic     phonetic sound
        sound_file   name of audio file
        sound_dir    path to the (optional) directory of sound files
        """

        if sound_dir is None:
            sound_dir = Vowel.DefaultSoundsDir

        super().__init__(glyph, example, description, translit, phonetic,
                         sound_file, sound_dir)

    def __str__(self):
        return f'<Vowel: {self.glyph}.{self.example} {self.translit}>'

    def __repr__(self):
        return self.__str__()


# dictionary mapping acrophonic transliteration to Consonant instance
#         glyph, example, description, translit, phonetic, meaning, initial, final, sound_file, sound_dir):
ThaiConsonants = {
                  'gor_gai': Consonant('ก', 'ไก่', 'MC', 'gor.gai', '', 'chicken', 'g-', '-k', 'gor_gai.wav'),
                  'kor_khai': Consonant('ข', 'ไข่', 'HC', 'kor.khai', '', 'egg', 'kh-', '-k', 'kor_khai.wav'),
                  'khor_khuaat': Consonant('ฃ', 'ขวด', 'HC', 'khor.khuaat', '', 'bottle', 'kh-', '-k', 'khor_khuaat.wav'),
                  'kor_khwaai': Consonant('ค', 'ควาย', 'LC2', 'kor.khwaai', '', 'buffalo', 'kh-', '-k', 'kor_khwaai.wav'),
                  'khor_khohn': Consonant('ฅ', 'คน', 'LC1', 'khor.khohn', '', 'person', 'kh-', '-k', 'khor_khohn.wav'),
                  'khor_rakhang': Consonant('ฆ', 'ระฆัง', 'LC2', 'khor.rakhang', '', 'bell', 'kh-', '-k', 'khor_rakhang.wav'),
                  'ngor_nguu': Consonant('ง', 'งู', 'LC1', 'ngor.nguu', '', 'snake', 'ng-', '-ng', 'ngor_nguu.wav'),
                  'jor_jaan': Consonant('จ', 'จาน', 'MC', 'jor.jaan', '', 'plate', 'j-', '-t', 'jor_jaan.wav'),
                  'chor_ching': Consonant('ฉ', 'ฉิ่ง', 'HC', 'chor.ching', '', 'cymbals', 'ch-', '.', 'chor_ching.wav'),
                  'chor_chaang': Consonant('ช', 'ช้าง', 'LC2', 'chor.chaang', '', 'elephant', 'ch-', '-t', 'chor_chaang.wav'),
                  'sor_so': Consonant('ซ', 'โซ่', 'LC2', 'sor.so', '', 'chain for animals', 's-', '-s', 'sor_so.wav'),
                  'chor_chuuhr': Consonant('ฌ', 'เฌอ', 'LC1', 'chor.chuuhr', '', 'tree', 'ch-', '-t', 'chor_chuuhr.wav'),
                  'yor_ying': Consonant('ญ', 'หญิง', 'LC1', 'yor.ying', '', 'woman', 'y-', '-n', 'yor_ying.wav'),
                  'dor_chadaa': Consonant('ฎ', 'ชฎา', 'MC', 'dor.chadaa', '', 'headdress', 'd-', '-t', 'dor_chadaa.wav'),
                  'tor_bpadtak': Consonant('ฏ', 'ปฏัก', 'MC', 'tor.bpadtak', '', 'javelin', 'dt-', '-t', 'tor_bpadtak.wav'),
                  'thor_thaan': Consonant('ฐ', 'ฐาน', 'HC', 'thor.thaan', '', 'pedestal', 'th-', '-t', 'thor_thaan.wav'),
                  'thor_mohntho': Consonant('ฑ', 'มณโฑ', 'LC2', 'thor.mohntho', '', 'Ramayana character', 'th-', '-t', 'thor_mohntho.wav'),
                  'thor_phuuthao': Consonant('ฒ', 'ผู้เฒ่า', 'LC2', 'thor.phuuthao', '', 'elder', 'th-', '-t', 'thor_phuuthao.wav'),
                  'nor_naehn': Consonant('ณ', 'เณร', 'LC1', 'nor.naehn', '', 'novice monk', 'n-', '-n', 'nor_naehn.wav'),
                  'dor_dek': Consonant('ด', 'เด็ก', 'MC', 'dor.dek', '', 'child', 'd-', '-t', 'dor_dek.wav'),
                  'dtor_dtao': Consonant('ต', 'เต่า', 'MC', 'dtor.dtao', '', 'turtle', 'dt-', '-t', 'dtor_dtao.wav'),
                  'tor_thoong': Consonant('ถ', 'ถุง', 'HC', 'tor.thoong', '', 'sack', 'th-', '-t', 'tor_thoong.wav'),
                  'tor_thahaan': Consonant('ท', 'ทหาร', 'LC2', 'tor.thahaan', '', 'soldier', 'th-', '-t', 'tor_thahaan.wav'),
                  'thor_thohng': Consonant('ธ', 'ธง', 'LC2', 'thor.thohng', '', 'flag', 'th-', '-t', 'thor_thohng.wav'),
                  'nor_nuu': Consonant('น', 'หนู', 'LC1', 'nor.nuu', '', 'mouse', 'n-', '-n', 'nor_nuu.wav'),
                  'bor_baimaai': Consonant('บ', 'ใบไม้', 'MC', 'bor.baimaai', '', 'leaf', 'b-', '-p', 'bor_baimaai.wav'),
                  'bpor_bplaa': Consonant('ป', 'ปลา', 'MC', 'bpor.bplaa', '', 'fish', 'bp-', '-p', 'bpor_bplaa.wav'),
                  'por_pheung': Consonant('ผ', 'ผึ้ง', 'HC', 'por.pheung', '', 'bee', 'ph-', '.', 'por_pheung.wav'),
                  'for_faa': Consonant('ฝ', 'ฝา', 'HC', 'for.faa', '', 'lid', 'f-', '.', 'for_faa.wav'),
                  'por_phaan': Consonant('พ', 'พาน', 'LC2', 'por.phaan', '', 'offering tray', 'ph-', '-p', 'por_phaan.wav'),
                  'for_fan': Consonant('ฟ', 'ฟัน', 'LC2', 'for.fan', '', 'tooth', 'f-', '-p', 'for_fan.wav'),
                  'phor_samphao': Consonant('ภ', 'สำเภา', 'LC2', 'phor.samphao', '', 'Chinese junk', 'ph-', '-p', 'phor_samphao.wav'),
                  'mor_maa': Consonant('ม', 'ม้า', 'LC1', 'mor.maa', '', 'horse', 'm-', '-m', 'mor_maa.wav'),
                  'yor_yak': Consonant('ย', 'ยักษ์', 'LC1', 'yor.yak', '', 'demon', 'y-', '.', 'yor_yak.wav'),
                  'ror_reuua': Consonant('ร', 'เรือ', 'LC1', 'ror.reuua', '', 'boat', 'r-', '-n', 'ror_reuua.wav'),
                  'lor_ling': Consonant('ล', 'ลิง', 'LC1', 'lor.ling', '', 'monkey', 'l-', '-n', 'lor_ling.wav'),
                  'wor_waaen': Consonant('ว', 'แหวน', 'LC1', 'wor.waaen', '', 'ring', 'w-', '.', 'wor_waaen.wav'),
                  'sor_saalaa': Consonant('ศ', 'ศาลา', 'HC', 'sor.saalaa', '', 'pavilion', 's-', '-t', 'sor_saalaa.wav'),
                  'sor_reuusee': Consonant('ษ', 'ฤๅษี', 'HC', 'sor.reuusee', '', 'hermit', 's-', '-t', 'sor_reuusee.wav'),
                  'sor_seuua': Consonant('ส', 'เสือ', 'HC', 'sor.seuua', '', 'tiger', 's-', '-t', 'sor_seuua.wav'),
                  'hor_heep': Consonant('ห', 'หีบ', 'HC', 'hor.heep', '', 'box', 'h-', '.', 'hor_heep.wav'),
                  'lor_joolaa': Consonant('ฬ', 'จุฬา', 'LC1', 'lor.joolaa', '', 'kite', 'l-', '-n', 'lor_joolaa.wav'),
                  'or_aang': Consonant('อ', 'อ่าง', 'MC', 'or.aang', '', 'basin', '.', '.', 'or_aang.wav'),
                  'hor_nohkhuuk': Consonant('ฮ', 'นกฮูก', 'LC2', 'hor.nohkhuuk', '', 'owl', 'h-', '.', 'hor_nohkhuuk.wav'),
                 }

# dictionary mapping acrophonic transliteration to Vowel instance
#                      glyph, example, description, translit, phonetic, sound_file, sound_dir=None):
ThaiVowels = {
              'aa': Vowel('◌า', '◌า', 'long vowel', 'aa', 'a:', 'aa.wav'),
              'ee': Vowel('◌ี', '◌ี', 'long vowel', 'ee', 'i:', 'ee.wav'),
              'oo': Vowel('◌ู', '◌ู', 'long vowel', 'oo', 'u:', 'oo.wav'),
              'ey': Vowel('เ◌', 'เ◌', 'long vowel', 'əy', 'e:', 'ey.wav'),
              'air': Vowel('แ◌', 'แ◌', 'long vowel', 'air', 'ε:', 'air.wav'),
              'oh': Vowel('โ◌', 'โ◌', 'long vowel', 'oh', 'ɔ:', 'oh.wav'),
              'oh': Vowel('◌อ', '◌อ', 'long vowel', 'oh', 'ɔ:', 'oh.wav'),
             }

DottedCircle = '◌'
Epsilon = 'ε'
UpsidedownE = 'ə'

if __name__ == '__main__':
    from playsound import playsound

    for (e_acro, cons) in ThaiConsonants.items():
        print(f"{cons.glyph}: {cons} ({e_acro})")
        cons.say()

#    for (e_acro, vow) in ThaiVowels.items():
#        print(f"{vow.glyph}: {vow} ({e_acro})")
#        vow.say()
