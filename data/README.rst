Data
====

This directory holds various data files for the ThaiFlash application:

consonants
----------

Audio files for each of the Thai consonants.

The data here was gathered from Google Translate.  The subdirectory
`consonants/create` contains all raw data and code used to create the 
consonant sound files.


