datafile = 'raw_consonants.dat'

# columns
symbol = 1
acrophonic = 4

delim = ':::'

with open(datafile) as fd:
    for line in fd:
        line = line.split()
        print(f"{line[symbol]} {line[acrophonic]}{delim}")
print()
