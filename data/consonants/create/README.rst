This directory contains raw data and the code used to produce the
consonant sounds in the parent directory.

The initial raw data was captured from the ebook "Read Thai in 10 days"
by Arthit Juyaso, Appendix II.

raw_consonants.dat
------------------

A lightly edited text file captured from the book.  Changes were::

* Replaced spaces in `(...)` with underscores
* Replaced `n/a` in "initial/final sounds" with "."
* Replaced spaces between two or more "transliteration+tones" fields with
  underscores
* Removed various characters that aren't required, like footnote markers, etc

A sample of the beginning of the file::

    1       ก กอ gaawM      ไก่   gaiL (chicken)     mid     g-      -k
    2       ข ขอ khaawR     ไข่  khaiL (egg) high    kh-     -k
    3       ฃ ฃอ khaawR     ขวด  khuaatL (bottle)   high    kh-     -k

This file still has consonant numbers and random numbers of spaces between
fields.


consonants.dat
--------------

This file is produced from `raw_consonants.dat` by the command::

    python3 raw2consonants.py > consonants.dat

The program removes the unwanted fields and produces output with the wanted
fields separated by the TAB character.

To get the raw sound data open a browser to "translate.google.com", select
Thai to English and then press the "listen to" speaker in the Thai part.

To capture the sounds to a file, run Quicktine "New audio recording", select
Soundflower 2ch source.  Set the system output to Soundflower 2ch.  Press record
in Quicktime and then the speaker button in the browser.  End the recoding
when the brower is finished.

Use Audacity to split the audio into \*.mp3 files in .. .

The *make_waves.sh* program converts the .mp3 files to .wav format and moves the
.wav files to the correct place in the *../gui* directory.


acrophonics.dat
---------------

This file contains just the acrophonics for each consonant, one line per
consonant.  Each line is formatted with extra characters to get Google Translate
to pronounce each acrophonic with the right spacing and pace.

The file is produced by executing::

    python3 consonants2acrophonic.py > acrophonics.dat

The data in the file is pasted into Google Translate which pronounces each line
and the sound is captured in the file `raw_consonants.m4a`.  This file is edited
with Audacity into files for each consonant sound.  Files are in the directory
one level up.
