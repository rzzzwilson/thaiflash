datafile = 'raw_consonants.dat'

# columns
symbol = 1
acrophonic = 4
translit_tone = 5
meaning = 6
class_ = 7
begin_sound = 8
end_sound = 9

with open(datafile) as fd:
    for line in fd:
        line = line.split()
        print(f"{line[symbol]}\t{line[acrophonic]}\t{line[translit_tone]}\t"
              f"{line[meaning]}\t{line[class_]}\t{line[begin_sound]}\t{line[end_sound]}")
