datafile = 'consonants.dat'

# This maps the acrophonix word to the prefix sound
acro2prefix = {'gai': 'gor',
               'khai': 'kor',
               'khuaat': 'khor',
               'khwaai': 'kor',
               'khohn': 'khor',        # obsolete
               'rakhang': 'khor',
               'nguu': 'ngor',
               'jaan': 'jor',
               'ching': 'chor',
               'chaang': 'chor',
               'so': 'sor',
               'chuuhr': 'chor',
               'ying': 'yor',
               'chadaa': 'dor',
               'bpadtak': 'tor',
               'thaan': 'thor',
               'mohntho': 'thor',
               'phuuthao': 'thor',
               'naehn': 'nor',
               'dek': 'dor',
               'dtao': 'dtor',
               'thoong': 'tor',
               'thahaan': 'tor',
               'thohng': 'thor',
               'nuu': 'nor',
               'baimaai': 'bor',
               'bplaa': 'bpor',
               'pheung': 'por',
               'faa': 'for',
               'phaan': 'por',
               'fan': 'for',
               'samphao': 'phor',
               'maa': 'mor',
               'yak': 'yor',
               'reuua': 'ror',
               'ling': 'lor',
               'waaen': 'wor',
               'saalaa': 'sor',
               'reuusee': 'sor',
               'seuua': 'sor',
               'heep': 'hor',
               'joolaa': 'lor',
               'aang': 'or',
               'nohkhuuk': 'hor',
              }

# read data into a list of lists
data = []
with open(datafile) as fd:
    line_num = 0
    for line in fd:
        line_num += 1
        fields = line.split()
        if len(fields) != 7:
            print(f'Error: line {line_num} has {len(fields)} fields?')
            exit()
        data.append(fields)

# process each line into a class definition
for line in data:
    (glyph, example, trans_class, meaning, tone, initial_sound, final_sound) = line

    tmp = trans_class.split('_')
    trans_class = []
    for t in tmp:
        trans = t[:-1]
        trans_class.append(trans)
    tl_example = ''.join(trans_class)

    meaning = meaning[1:-1]
    tmp = meaning.split(',_')
    meaning = []
    for m in tmp:
        m = m.replace('_', ' ')
        meaning.append(m)

    tl_glyph = acro2prefix[tl_example]
    sound_file = f'{tl_glyph}_{tl_example}.mp3'

    print(f"              '{tl_glyph}_{tl_example}': Consonant('{glyph}', "
          f"'{example}', '{tl_glyph}', '{tl_example}', {meaning}, '{tone}', "
          f"'{initial_sound}', '{final_sound}', '{sound_file}'),")
